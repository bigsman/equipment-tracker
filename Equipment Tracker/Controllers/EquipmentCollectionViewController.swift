//
//  EquipmentCollectionViewController.swift
//  Equipment Tracker
//
//  Created by Sohel Seedat on 17/11/2015.
//  Copyright © 2015 BigSMan. All rights reserved.
//

import UIKit

class EquipmentCollectionViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    @IBOutlet var collectionView: UICollectionView!
    
    var equipment = [Equipment]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Equipment Inventory"
    }
    
    
    // MARK:- UICollectionViewDataSource
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return equipment.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("EquipmentCell", forIndexPath: indexPath) as! EquipmentCollectionViewCell
        let equipmentItem = equipment[indexPath.row]
        cell.nameLabel.text = equipmentItem.name
        return cell
    }

    // MARK:- IBActions

    @IBAction func addButtonPressed(sender: AnyObject) {

    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "AddEquipmentSegue" {
//            let detailVC = segue.destinationViewController as! AddEquipmentViewController
//            detailVC.equipment = equipment[(self.collectionView.indexPathsForSelectedItems()?.first?.row)!]
        }
    }

}
