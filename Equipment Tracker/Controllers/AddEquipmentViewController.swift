//
//  AddEquipmentViewController.swift
//  Equipment Tracker
//
//  Created by Sohel Seedat on 17/11/2015.
//  Copyright © 2015 BigSMan. All rights reserved.
//

import UIKit

class AddEquipmentViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet var addPhotoButton: UIButton!

    @IBOutlet var imageView: UIImageView!
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var brandTextField: UITextField!
    @IBOutlet var serialNumberTextField: UITextField!
    @IBOutlet var datePurchasedTextField: UITextField!
    
    let datePickerView = UIDatePicker()

    var equipment: Equipment!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTextFields()
    }
    
    func setupTextFields() {
        self.datePickerView.datePickerMode = UIDatePickerMode.Date
        self.datePurchasedTextField.inputView = self.datePickerView
        self.datePickerView.addTarget(self, action: Selector("handleDatePicker:"), forControlEvents: UIControlEvents.ValueChanged)
    }
    
    func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = .ShortStyle
        self.datePurchasedTextField.text = dateFormatter.stringFromDate(sender.date)
    }
    
    // MARK:- IBActions
    
    @IBAction func addPhotoButtonPressed(sender: AnyObject) {
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        presentViewController(picker, animated: true, completion: nil)
    }
    
    @IBAction func cancelButtonPressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func saveButtonPressed(sender: AnyObject) {
        self.equipment = Equipment(name: nameTextField.text!, brand: brandTextField.text!)
        
        if let serialNumber = serialNumberTextField.text {
            self.equipment.serialNumber = serialNumber
        }
        if let _ = datePurchasedTextField.text {
            self.equipment.datePurchased = self.datePickerView.date
        }
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK:- UIImagePickerController
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }

}
