//
//  EquipmentCollectionViewCell.swift
//  Equipment Tracker
//
//  Created by Sohel Seedat on 17/11/2015.
//  Copyright © 2015 BigSMan. All rights reserved.
//

import UIKit

class EquipmentCollectionViewCell: UICollectionViewCell {
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!    
}
