//
//  Equipment.swift
//  Equipment Tracker
//
//  Created by Sohel Seedat on 17/11/2015.
//  Copyright © 2015 BigSMan. All rights reserved.
//

import UIKit

class Equipment: NSObject {
    var name: String
    var brand: String
    var serialNumber: String?
    var datePurchased: NSDate?
    var age: Double?
    var images: [String] = []
    
    init(name: String, brand: String) {
        self.name = name
        self.brand = brand
    }
}
